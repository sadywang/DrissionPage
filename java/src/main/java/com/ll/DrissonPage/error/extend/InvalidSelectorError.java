package com.ll.DrissonPage.error.extend;

import com.ll.DrissonPage.error.BaseError;

/**
 * @author 陆
 * @address <a href="https://t.me/blanksig"/>click
 */
public class InvalidSelectorError extends BaseError {
    public InvalidSelectorError(String info) {
        super(info);
    }
}
