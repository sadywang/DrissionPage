package com.ll.DrissonPage.units.listener;

import java.util.Map;

/**
 * @author 陆
 * @address <a href="https://t.me/blanksig"/>click
 */
public class ResponseExtraInfo  extends ExtraInfo{
    public ResponseExtraInfo(Map<String, Object> extraInfo) {
        super(extraInfo);
    }
}
